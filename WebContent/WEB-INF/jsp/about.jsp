<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="pathUrl" value="${pageContext.request.contextPath}/"/>
<!DOCTYPE html>
<!--[if IE 7 ]><html class="ie ie7 lte9 lte8 lte7" lang="en-US"><![endif]-->
<!--[if IE 8]><html class="ie ie8 lte9 lte8" lang="en-US">	<![endif]-->
<!--[if IE 9]><html class="ie ie9 lte9" lang="en-US"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="noIE" lang="en-US">
<!--<![endif]-->
	<head>
		<title><spring:message code="site.title"></spring:message></title>

		<!-- meta -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no"/>
		

		<!-- google fonts -->
		<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=PT+Sans'>
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Droid+Serif:regular,bold"/>
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Alegreya+Sans:regular,italic,bold,bolditalic"/>
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Nixie+One:regular,italic,bold,bolditalic"/>
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Alegreya+SC:regular,italic,bold,bolditalic"/>
		

<spring:url value="/resources/assets/css/bootstrap.min.css" var="bootstrapCSS"></spring:url>
<spring:url value="/resources/assets/css/font-awesome.min.css" var="fontCSS"></spring:url>
<spring:url value="/resources/assets/css/style.css" var="styleCSS"></spring:url>

<!-- Images -->
<spring:url value="/resources/assets/images/testimonial_31-190x190.jpg" var="enesImage"></spring:url>
<spring:url value="/resources/assets/images/testimonial_22-190x190.jpg" var="abdullahImage"></spring:url>

		<spring:url value="/resources/assets/js/jquery-2.1.3.min.js" var="jqueryMinJS"></spring:url>
<spring:url value="/resources/assets/js/bootstrap.min.js" var="bootstrapMinJS"></spring:url>
<spring:url value="/resources/assets/js/jquery-migrate-1.2.1.min.js" var="jqueryMigrateJS"></spring:url>
<spring:url value="/resources/assets/js/jquery.actual.min.js" var="jqueryActualJS"></spring:url>
<spring:url value="/resources/assets/js/jquery.scrollTo.min.js" var="jqueryScrollJS"></spring:url>
<spring:url value="/resources/assets/js/bootstrap.js" var="bootstrapJS"></spring:url>
<spring:url value="/resources/assets/js/npm.js" var="npmJS"></spring:url>
<spring:url value="/resources/assets/js/script.js" var="scriptJS"></spring:url>


		<!-- css -->
		<link href="${bootstrapCSS}" rel="stylesheet">
		<link href="${fontCSS}" rel="stylesheet" >
		<link href="${styleCSS}" rel="stylesheet" media="screen"/>
		

		

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.js"></script>
			<script src="assets/js/respond.js"></script>
		<![endif]-->

		<!--[if IE 8]>
	    	<script src="assets/js/selectivizr.js"></script>
	    <![endif]-->
	</head>
	
	<body>
	
	<div class="content-block" id="testimonials">
				<div class="container">
					<header class="block-heading cleafix">
						<h1>Ekibimiz</h1>
						<p>Some happy customers have to say.</p>
					</header>
					<section class="block-body">
						<div class="row">
							<div class="col-md-4">
								<div class="testimonial">
									<img src="${abdullahImage}">
									<p>In at accumsan risus. Nam id volutpat ante. Etiam vel mi mattis, vulputate nunc nec, sodales nibh. Etiam nulla magna, gravida eget ultricies sit amet, hendrerit in lorem.</p>
									<strong>Jhon Doe</strong><br/>
									<span>Head of Ideas, Technext</span>
								</div>
							</div>

							<div class="col-md-4">
								<div class="testimonial">
									<img src="${enesImage}">
									<p>In at accumsan risus. Nam id volutpat ante. Etiam vel mi mattis, vulputate nunc nec, sodales nibh. Etiam nulla magna, gravida eget ultricies sit amet, hendrerit in lorem.</p>
									<strong>Albert Doe</strong><br/>
									<span>Team Lead, Design Studio</span>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div><!-- /#testimonials -->


      <script src="${jqueryMinJS}"></script>
		<script src="${jqueryMigrateJS}"></script>
		<script src="${bootstrapMinJS}"></script>
		<script src="${jqueryActualJS}"></script>
		<script src="${jqueryScrollJS}"></script>
		<script src="${bootstrapMinJS}"></script>
		<script src="${bootstrapJS}"></script>
		<script src="${npmJS}"></script>
		<script src="${scriptJS}"></script>
	</body>
</html>
