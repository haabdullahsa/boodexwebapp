<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="pathUrl" value="${pageContext.request.contextPath}/"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>Books</title>
<script src="https://code.jquery.com/jquery-3.1.1.js" integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA=" crossorigin="anonymous"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.css"/>
 
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.js"></script>


<spring:url value="/resources/assets/images/book_default.png" var="bookDefault"></spring:url>
<spring:url value="/resources/assets/images/arrow_down.png" var="arrowImage"></spring:url>

<meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no"/>
		

		<!-- google fonts -->
		<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=PT+Sans'>
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Droid+Serif:regular,bold"/>
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Alegreya+Sans:regular,italic,bold,bolditalic"/>
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Nixie+One:regular,italic,bold,bolditalic"/>
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Alegreya+SC:regular,italic,bold,bolditalic"/>
		
	
 
		<spring:url value="/resources/assets/js/jquery-2.1.3.min.js" var="jqueryMinJS"></spring:url>
<spring:url value="/resources/assets/js/bootstrap.min.js" var="bootstrapMinJS"></spring:url>
<spring:url value="/resources/assets/js/jquery-migrate-1.2.1.min.js" var="jqueryMigrateJS"></spring:url>
<spring:url value="/resources/assets/js/jquery.actual.min.js" var="jqueryActualJS"></spring:url>
<spring:url value="/resources/assets/js/jquery.scrollTo.min.js" var="jqueryScrollJS"></spring:url>
<spring:url value="/resources/assets/js/bootstrap.js" var="bootstrapJS"></spring:url>
<spring:url value="/resources/assets/js/npm.js" var="npmJS"></spring:url>
<spring:url value="/resources/assets/js/script.js" var="scriptJS"></spring:url>

<spring:url value="/resources/assets/css/bootstrap.min.css" var="bootstrapCSS"></spring:url>
<spring:url value="/resources/assets/css/font-awesome.min.css" var="fontCSS"></spring:url>
<spring:url value="/resources/assets/css/style.css" var="styleCSS"></spring:url>

<!-- css -->
		<link href="${bootstrapCSS}" rel="stylesheet">
		<link href="${fontCSS}" rel="stylesheet" >
		<link href="${styleCSS}" rel="stylesheet" media="screen"/>
		
	</head>
<body>
 
 <div style="margin-left: 15px; margin-top: 20px;"><h4>${book.name}</h4></div>
 <div style="margin-left: 15px;"><a href=<c:url value="/author"></c:url> class="btn btn-danger btn-sm"><i class="fa fa-arrow-left fa"></i>&nbsp;Geri</a></div>
 <form class="container" action="createContent?id=${book.id}" method="POST" style="margin-top: 20px; margin-bottom: 15px; text-align: center;">
 <div class="row">
   <div class="col-xs-4">
    <input id="contentField" name="content" type="text" class="form-control" placeholder="İçerik" required>
  </div>
  <div class="col-xs-2">
    <input id="firstPageField" name="firstPageNo" type="text" class="form-control" placeholder="İlk Sayfa No" required>
  </div>
  <div class="col-xs-2">
    <input id="lastPageField"  name="lastPageNo" type="text" class="form-control" placeholder="Son Sayfa No" required>
  </div>
  <div class="col-xs-2">
    <button type="submit" id="newContent" class="btn btn-primary btn-sm" ><i class="fa fa-plus-circle fa"></i>&nbsp;Ekle</button>
  </div>
</div>

 </form>

<table id="contentList" class="table table-striped table-bordered" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Content</th>
                <th>First Page No</th>
                <th>Last Page No</th>
                <th><i class="fa fa-eye fa"></i>&nbsp;Preview</th>
                 <th></th>
            </tr>
        </thead>
        <tbody>
        <c:if test="${not empty contents}">
        <c:forEach items="${contents}" var="content">
        <form action="updateContent/${book.id}/update?id=${content.id}" method="POST" >
        <tr>
        
                <th> 
    <input id="contentField" name="content" type="text" class="form-control" placeholder="İçerik" value="${content.subjectName}" required>
    
</th>
                <th>
    <input id="contentField" name="firstPageNo" type="text" class="form-control" placeholder="İlk Sayfa No" value="${content.minPageNumber}" required>
    
    </th>
                <th>
    <input id="contentField" name="lastPageNo" type="text" class="form-control" placeholder="Son Sayfa No" value="${content.maxPageNumber}" required></th>
                <th>${content.previewCount}</th>
                 <th> <td><button type="submit" class="btn btn-success btn-sm"><i class="fa fa-pencil fa"></i>&nbsp;Güncelle</button>&nbsp;<a href=<c:url value="/deleteContent/${book.id}/delete?id=${content.id}"></c:url> class="btn btn-danger btn-sm"><i class="fa fa-trash-o fa"></i>&nbsp;Sil</a></td></th>
            
            </tr>
            </form>
            </c:forEach>
                </c:if>
        </tbody>
        <tfoot>
            <tr>
            <th>Content</th>
                <th>First Page No</th>
                <th>Last Page No</th>
                <th>Preview</th>
                 <th></th>
            </tr>
        </tfoot>
    </table>
  
 
   
<script src="${jqueryMinJS}"></script>
		<script src="${jqueryMigrateJS}"></script>
		<script src="${bootstrapMinJS}"></script>
		<script src="${jqueryActualJS}"></script>
		<script src="${jqueryScrollJS}"></script>
		<script src="${bootstrapMinJS}"></script>
		<script src="${bootstrapJS}"></script>
		<script src="${npmJS}"></script>
		<script src="${scriptJS}"></script>
		

		<script type="text/javascript">
		
		var editor; // use a global for the submit and return data rendering in the examples
		 
		$(document).ready(function() {
		   
		 
			  $('#contentList').DataTable();
		 
		});</script>
		
		

</body>
</html>