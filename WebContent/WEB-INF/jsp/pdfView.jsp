<%@page import="com.boodex.domain.Author"%>
<%@page import="org.springframework.security.crypto.codec.Base64"%>
<%@page import="com.boodex.domain.Book"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="pathUrl" value="${pageContext.request.contextPath}/"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>PDF</title>
</head>
<body>
<%
Book book=(Book)request.getAttribute("book");
byte[] pdf =book.getBookFile();

response.setHeader("Expires", "0");
response.setHeader("Cache-Control", "must-revalidate, post-check=0, pre-check=0");
response.setHeader("Pragma", "public");

response.setHeader("Content-Disposition", "inline; filename="+book.getName()+".pdf");
response.setContentType("application/pdf");
response.setContentLength(pdf.length);
response.getOutputStream().write(pdf);
response.getOutputStream().flush();
%>

<script type="text/javascript">

</script>
</body>
</html>