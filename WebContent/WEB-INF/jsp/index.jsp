<%@page import="com.boodex.domain.Content"%>
<%@page import="com.boodex.domain.Author"%>
<%@page import="org.springframework.security.crypto.codec.Base64"%>
<%@page import="com.boodex.domain.Book"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="pathUrl" value="${pageContext.request.contextPath}/"/>

<!DOCTYPE html>
<!--[if IE 7 ]><html class="ie ie7 lte9 lte8 lte7" lang="en-US"><![endif]-->
<!--[if IE 8]><html class="ie ie8 lte9 lte8" lang="en-US">	<![endif]-->
<!--[if IE 9]><html class="ie ie9 lte9" lang="en-US"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="noIE" lang="en-US">
<!--<![endif]-->
	<head>
		<title><spring:message code="site.title"></spring:message></title>

		<!-- meta -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		
		<meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no"/>
		

		<!-- google fonts -->
		<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=PT+Sans'>
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Droid+Serif:regular,bold"/>
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Alegreya+Sans:regular,italic,bold,bolditalic"/>
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Nixie+One:regular,italic,bold,bolditalic"/>
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Alegreya+SC:regular,italic,bold,bolditalic"/>
		


<spring:url value="/resources/assets/css/bootstrap.min.css" var="bootstrapCSS"></spring:url>
<spring:url value="/resources/assets/css/font-awesome.min.css" var="fontCSS"></spring:url>
<spring:url value="/resources/assets/css/style.css" var="styleCSS"></spring:url>

<!-- Images -->
<spring:url value="/resources/assets/images/arrow_down.png" var="arrowImage"></spring:url>
<spring:url value="/resources/assets/images/rain_1-320x240.jpg" var="booksTestImage"></spring:url>

		<spring:url value="/resources/assets/js/jquery-2.1.3.min.js" var="jqueryMinJS"></spring:url>
<spring:url value="/resources/assets/js/bootstrap.min.js" var="bootstrapMinJS"></spring:url>
<spring:url value="/resources/assets/js/jquery-migrate-1.2.1.min.js" var="jqueryMigrateJS"></spring:url>
<spring:url value="/resources/assets/js/jquery.actual.min.js" var="jqueryActualJS"></spring:url>
<spring:url value="/resources/assets/js/jquery.scrollTo.min.js" var="jqueryScrollJS"></spring:url>
<spring:url value="/resources/assets/js/bootstrap.js" var="bootstrapJS"></spring:url>
<spring:url value="/resources/assets/js/npm.js" var="npmJS"></spring:url>
<spring:url value="/resources/assets/js/script.js" var="scriptJS"></spring:url>


		<!-- css -->
		<link href="${bootstrapCSS}" rel="stylesheet">
		<link href="${fontCSS}" rel="stylesheet" >
		<link href="${styleCSS}" rel="stylesheet" media="screen"/>
		

		

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.js"></script>
			<script src="assets/js/respond.js"></script>
		<![endif]-->

		<!--[if IE 8]>
	    	<script src="assets/js/selectivizr.js"></script>
	    <![endif]-->
	</head>
	
	<body>
		<div id="drawer-right">
			<div class="cross text-right">
				<a class="toggleDrawer" href="#"><i class="fa fa-times-circle fa-2x"></i></a>
			</div>
			<h2><spring:message code="drawer.pages"></spring:message></h2>
			<nav>
				<ul class="nav nav-pills nav-stacked">
					<li>
						<a href="#wrapper"><i class="fa fa-home"></i> <spring:message code="drawer.home"></spring:message></a>
					</li>
					<li>
						<a href="#portfolio"><i class="fa fa-users"></i> <spring:message code="drawer.authors"></spring:message></a>
					</li>
					<li>
						<a href="#contact"><i class="fa fa-phone-square"></i> <spring:message code="drawer.contact"></spring:message></a>
					</li>
				</ul>
			</nav>
			<div class="social">
				<h2><spring:message code="drawer.stayconnected"></spring:message></h2>
				<ul>
					<li><a href=""><i class="fa fa-facebook-square fa-3x"></i></a></li>
					<li><a href=""><i class="fa fa-twitter-square fa-3x"></i></a></li>
					<li><a href=""><i class="fa fa-tumblr-square fa-3x"></i></a></li>
					<li><a href=""><i class="fa fa-google-plus-square fa-3x"></i></a></li>
				</ul>
			</div>
		</div><!-- #drawer-right -->

		<div id="wrapper">
			
			<div id="header" class="content-block">
				<section class="top clearfix">
					<div class="pull-left">
						<h1><a class="logo" style="text-decoration: none;">Boodex <span><a style="font-size: large; color:#fff; text-decoration: none;margin-top: -10px;" href="?language=en">en</a><a style="text-decoration:none; font-size:x-large; color:#fff;;margin-top: -10px;">&nbsp;|&nbsp;</a><a style="font-size: large; color:#fff; text-decoration: none;;margin-top: -10px;" href="?language=tr">tr</a></span></a></h1>
					</div>
					<div class="pull-right">
					
				<c:if test="${not empty user}">
				<div style="font-size:12px; margin-top:-10px; margin-bottom:6px; margin-right: 12px; color:white; text-decoration: none;"><form action="logout" method="POST">Hoşgeldiniz, Sayın ${user.nameSurname}&nbsp;<button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-sign-out fa"></i>&nbsp;Çıkış</button></form></div>
				</c:if>
					<c:if test="${empty user}">
					<a href="#" class="dropdown-toggle btn btn-primary" data-toggle="dropdown" style="margin-top: -10px;"><span class="fa fa-sign-in"> </span>&nbsp;<spring:message code="signin.text"></spring:message></a> &nbsp;
			
							<ul id="login-dp" class="dropdown-menu" style="margin-right: 10px; margin-top:-12px; padding: 15px;">
				<li>
					 <div class="row">
							<div class="col-md-12">
								 <form class="form" role="form" method="POST" action="login" accept-charset="UTF-8" id="login-nav">
										<div class="form-group">
											 
											 <input type="email" class="form-control" name="email" id="email"  placeholder="<spring:message code="signin.login.email"></spring:message>" required>
										</div>
										<div class="form-group">
							
											 <input type="password" class="form-control" name="password" id="password" placeholder="<spring:message code="signin.login.password"></spring:message>" required>
                                             <div class="help-block text-right"><a href=""><spring:message code="signin.login.forgetpass"></spring:message></a></div>
										</div>
										<div class="form-group">
											 <button type="submit" class="btn btn-primary btn-block"><spring:message code="signin.login.text"></spring:message></button>
										</div>
								 </form>
							</div>
								
							<div class="bottom text-center">
							<spring:message code="signin.login.register.text"></spring:message> <a href=<c:url value="/register"></c:url>><b><spring:message code="signin.login.register.join"></spring:message></b></a>
							<p>
							 <a href=<c:url value="/author"/>><spring:message code="author.login.text"></spring:message></a>
							</p>
							</div>
					 </div>
				</li>
			</ul>
			</c:if>
						<a class="toggleDrawer" href="#"><i class="fa fa-bars fa-2x"></i></a>
					</div>
				</section>
				<section class="center">
					<div class="slogan">
						<spring:message code="page.header.title"></spring:message>
					</div>
					<div class="secondary-slogan">
						<!--<spring:message code="page.header.subtitle"></spring:message>-->
						<p></p>
						<p></p>
				<div class="container">		
      
    </div>
  </div>
  <form class="form-horizontal" role="form"  action="getListByFilter" method="post">
		<div class="row">
		<div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4">
            <div class="input-group" id="adv-search">
                <input type="text" class="form-control input-lg" name="key" placeholder="<spring:message code="page.search.placeholder"></spring:message>" />
                <div class="input-group-btn">
                    <div class="btn-group" role="group">
                        <div class="dropdown dropdown-lg">
                            <button type="button" class="btn btn-default dropdown-toggle btn-lg" data-toggle="dropdown" aria-expanded="false"><span class="caret"></span></button>
                            <div class="dropdown-menu dropdown-menu-right" role="menu" style="width:250px;">
                                  <div class="form-group">
                                    <label for="filter" style="color:black"><spring:message code="page.search.filter"></spring:message></label>
                                    <select class="form-control" name="filter">
                                        <option value="0" selected><spring:message code="page.search.allofsearch"></spring:message></option>
                                        <option value="1"><spring:message code="page.search.best"></spring:message></option>
                                        <option value="2"><spring:message code="page.search.new"></spring:message></option>
                                    </select>
                                  </div>
                                  <div class="form-group">
                                    <label for="contain" style="color:black"><spring:message code="page.search.author"></spring:message></label>
                                    <input class="form-control" type="text" name="authorName" />
                                  </div>
                                  <div class="form-group">
                                    <label for="contain" style="color:black"><spring:message code="page.search.type"></spring:message></label>
                                    <input class="form-control" type="text" name="type" />
                                  </div>
                                  <button type="submit" class="btn btn-primary"><span class="fa fa-search" aria-hidden="true"></span></button>
                               
                            </div>
                        </div>
                        <button  type="submit" class="btn btn-primary btn-lg"><span class="fa fa-search" aria-hidden="true"></span></button></a>
                    </div>
                </div>
            </div>
          </div>
        </div>
        </form>
	</div>
      </div>
	  
	</div>
</div>
					</div>
				</section>
				<section class="bottom">
					<a id="scrollToContent" href="#">
						<img src="${arrowImage}"/>
					</a>
				</section>
			</div><!-- header -->
			
			
			<div class="content-block" id="books">
				<div class="container">
					<header class="block-heading cleafix">
						<a href="#" class="btn btn-o btn-lg pull-right"><spring:message code="page.book.turnsearch"></spring:message></a>
						<h1><spring:message code="page.book.result"></spring:message></h1>
						<p></p>
					</header>
					<section class="block-body">
					
						<div class="row">
						<c:forEach items="${books}" var="content">
							<div class="col-sm-4">		
							<%
                Content book=(Content)pageContext.getAttribute("content");
                byte[] encodeBase64 = Base64.encode(book.getBook().getImageData());
                String base64Encoded = new String(encodeBase64, "UTF-8");
                %>			
								<a href=<c:url value="/viewPdf?id=${content.book.id}#page=${content.minPageNumber}"></c:url> class="recent-work" style="background-image:url('data:image/jpeg;base64,<%=base64Encoded%>')">
								   <span style="text-decoration: none;color: white; font-size: large;">${content.book.name}</span>
								   <br>
								   <span style="text-decoration: none;color: white; font-size: medium;">${content.book.type}</span>
								      <p></p>
									<span class="btn btn-o-white"><spring:message code="page.book.seepages"></spring:message></span>
								</a>
							</div>
							
									</c:forEach>
						</div>
				
				
					
					</section>
				</div>
			</div><!-- #portfolio_books -->
			
			
			<div class="content-block" id="portfolio">
				<div class="container">
					<header class="block-heading cleafix">
						<h1><spring:message code="page.authors.title" ></spring:message></h1>
						<p></p>
					</header>
					<section class="block-body">
					<c:if test="${not empty authors}">
						<div class="row">
						<c:forEach items="${authors}" var="author">
							<div class="col-sm-4">
							<%
							Author author=(Author)pageContext.getAttribute("author");
							byte[] encodeBase64 = Base64.encode(author.getProfileImage());
			                String base64EncodedProfile = new String(encodeBase64, "UTF-8");
							%>
								<a href="#" class="recent-work" style="background-image:url('data:image/jpeg;base64,<%=base64EncodedProfile%>')">
									<span class="btn btn-o-white">${author.authorNameSurname}</span>
								</a>
							</div>
							</c:forEach>
						</div>
						
						</c:if>
					</section>
				</div>
			</div><!-- #portfolio -->

			<div class="content-block parallax" id="services">
				<div class="container text-center">
					<header class="block-heading cleafix">
						<h1><spring:message code="page.aboutsite.title"></spring:message></h1>
						<p><spring:message code="page.aboutsite.subtitle"></spring:message></p>
					</header>
					<section class="block-body">
						<div class="row">
							<div class="col-md-4">
								<div class="service">
									<i class="fa fa-search"></i>
									<h2><spring:message code="page.aboutsite.search.title"></spring:message></h2>
									<p><spring:message code="page.aboutsite.search.about"></spring:message></p>
								</div>
							</div>
							<div class="col-md-4">
								<div class="service">
									<i class="fa fa-book"></i>
									<h2><spring:message code="page.aboutsite.discover.title"></spring:message></h2>
									<p><spring:message code="page.aboutsite.discover.about"></spring:message></p>
								</div>
							</div>
							<div class="col-md-4">
								<div class="service">
									<i class="fa fa-download"></i>
									<h2><spring:message code="page.aboutsite.download.title"></spring:message></h2>
									<p><spring:message code="page.aboutsite.download.about"></spring:message></p>
								</div>
							</div>
						</div>
					</section>
				</div>
			</div><!-- #services -->




			<div class="content-block" id="contact">
				<div class="container text-center">
					<header class="block-heading cleafix">
						<h1><spring:message code="page.contact.title"></spring:message></h1>
						<p><spring:message code="page.contact.subtitle"></spring:message></p>
					</header>
					<section class="block-body">
						<div class="row">
							<div class="col-md-6 col-md-offset-3">
								<form class="" role="form">
									<div class="form-group">
								    	<input type="text" class="form-control form-control-white" id="subject" placeholder="<spring:message code="page.contact.name"></spring:message>" required>
								  	</div>
								    <div class="form-group">
								    	<input type="email" class="form-control form-control-white" id="exampleInputEmail2" placeholder="<spring:message code="page.contact.email"></spring:message>" required>
								    </div>
								    <div class="form-group">
								    	<textarea class="form-control form-control-white" placeholder="<spring:message code="page.contact.content"></spring:message>" required></textarea>
								    </div>
								  <button type="submit" class="btn btn-o-white"><i class="fa fa-paper-plane-o fa-2x"></i></button>
								</form>
							</div>
						</div>
					</section>
				</div>
			</div><!-- #contact -->

			<div class="content-block" id="footer">
				<div class="container">
					<div class="row">
						<div class="col-xs-6">&copy; Copyright Boodex 2016 </div>
						<div class="col-xs-6 text-right"> <a href=<c:url value="/about"/> target="_blank" style="color:#fff;text-decoration: none;" ><spring:message code="page.aboutus"></spring:message></a></div>
					</div>
				</div>
			</div><!-- #footer -->


		</div><!--/#wrapper-->
		



        <script src="${jqueryMinJS}"></script>
		<script src="${jqueryMigrateJS}"></script>
		<script src="${bootstrapMinJS}"></script>
		<script src="${jqueryActualJS}"></script>
		<script src="${jqueryScrollJS}"></script>
		<script src="${bootstrapMinJS}"></script>
		<script src="${bootstrapJS}"></script>
		<script src="${npmJS}"></script>
		<script src="${scriptJS}"></script>
		
		 
		
	</body>
</html>
