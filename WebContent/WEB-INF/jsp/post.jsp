<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="pathUrl" value="${pageContext.request.contextPath}/"/>
<!DOCTYPE html>
<!--[if IE 7 ]><html class="ie ie7 lte9 lte8 lte7" lang="en-US"><![endif]-->
<!--[if IE 8]><html class="ie ie8 lte9 lte8" lang="en-US">	<![endif]-->
<!--[if IE 9]><html class="ie ie9 lte9" lang="en-US"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html class="noIE" lang="en-US">
<!--<![endif]-->
	<head>
		<title><spring:message code="site.title"></spring:message></title>

		<!-- meta -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no"/>
		

		<!-- google fonts -->
		<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=PT+Sans'>
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Droid+Serif:regular,bold"/>
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Alegreya+Sans:regular,italic,bold,bolditalic"/>
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Nixie+One:regular,italic,bold,bolditalic"/>
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Alegreya+SC:regular,italic,bold,bolditalic"/>
		

<spring:url value="/resources/assets/css/bootstrap.min.css" var="bootstrapCSS"></spring:url>
<spring:url value="/resources/assets/css/font-awesome.min.css" var="fontCSS"></spring:url>
<spring:url value="/resources/assets/css/style.css" var="styleCSS"></spring:url>

<!-- Images -->
<spring:url value="/resources/assets/images/testimonial_31-190x190.jpg" var="enesImage"></spring:url>
<spring:url value="/resources/assets/images/testimonial_22-190x190.jpg" var="abdullahImage"></spring:url>

		<spring:url value="/resources/assets/js/jquery-2.1.3.min.js" var="jqueryMinJS"></spring:url>
<spring:url value="/resources/assets/js/bootstrap.min.js" var="bootstrapMinJS"></spring:url>
<spring:url value="/resources/assets/js/jquery-migrate-1.2.1.min.js" var="jqueryMigrateJS"></spring:url>
<spring:url value="/resources/assets/js/jquery.actual.min.js" var="jqueryActualJS"></spring:url>
<spring:url value="/resources/assets/js/jquery.scrollTo.min.js" var="jqueryScrollJS"></spring:url>
<spring:url value="/resources/assets/js/bootstrap.js" var="bootstrapJS"></spring:url>
<spring:url value="/resources/assets/js/npm.js" var="npmJS"></spring:url>
<spring:url value="/resources/assets/js/script.js" var="scriptJS"></spring:url>


		<!-- css -->
		<link href="${bootstrapCSS}" rel="stylesheet">
		<link href="${fontCSS}" rel="stylesheet" >
		<link href="${styleCSS}" rel="stylesheet" media="screen"/>
		

		

		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="assets/js/html5shiv.js"></script>
			<script src="assets/js/respond.js"></script>
		<![endif]-->

		<!--[if IE 8]>
	    	<script src="assets/js/selectivizr.js"></script>
	    <![endif]-->
	</head>
	
	<body>
	
	<div class="text-right" style="margin-right: 10px;">
	  <a style="font-size: large; color:#000; text-decoration: none;margin-top: -10px;" href="?language=en">en</a><a style="text-decoration:none; font-size:x-large; color:#000;;margin-top: -10px;">&nbsp;|&nbsp;</a><a style="font-size: large; color:#000; text-decoration: none;;margin-top: -10px;" href="?language=tr">tr</a>
	</div>
	<div class="content-block" id="testimonials">
				<div class="container">
					<header class="block-heading cleafix">
						<h1><spring:message code="register.text"></spring:message></h1>
						<p><spring:message code="register.subtext"></spring:message></p>
						
						<c:if test="${not empty success}">
						<div style="margin-top: 5px;" class="login-register">
						<h3><spring:message  code="register.youarememberanymore"></spring:message></h3>
						</div>
						</c:if>
						
					</header>
					<section class="block-body">
						<div class="main-login main-center">
						<form role="form" action="register" method="POST" class="form-horizontal" accept-charset="UTF-8" >
						
						<div class="form-group">
						<div class="cols-sm-6 col-md-4 col-md-offset-4">
						  <div class="input-group">
						       <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
						       <input type="text" class="form-control" name="name" id="name" placeholder="<spring:message code="register.name"></spring:message>"  required>
						  </div>
						</div>
						</div>
						
						<div class="form-group">
						<div class="cols-sm-6 col-md-4 col-md-offset-4">
						  <div class="input-group">
						       <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
						       <input type="email" class="form-control" name="email" id="email" placeholder="<spring:message code="register.mail"></spring:message>" required>
						  </div>
						</div>
						</div>
						
						<div class="form-group">
						<div class="cols-sm-6 col-md-4 col-md-offset-4">
						  <div class="input-group">
						       <span class="input-group-addon"><i class="fa fa-key fa" aria-hidden="true"></i></span>
						       <input type="password" class="form-control" name="password" id="password" placeholder="<spring:message code="register.password" ></spring:message>" required>
						  </div>
						</div>
						</div>
						
						
						<div class="form-group">
						<div class="cols-sm-6 col-md-4 col-md-offset-4">
						  <div class="input-group">
						       <span class="input-group-addon"><i class="fa fa-venus-mars fa" aria-hidden="true"></i></span>
						      <select class="form-control" name="genders" id="genders">
						      <option value="male"><spring:message code="register.gender.male"></spring:message></option>
						           <option value="female"><spring:message code="register.gender.female"></spring:message></option>
						      </select>
						  </div>
						</div>
						</div>
						
						<div class="form-group">
						<div class="cols-sm-6 col-md-4 col-md-offset-4">
						  <div class="input-group">
						       <span class="input-group-addon"><i class="fa fa-birthday-cake fa" aria-hidden="true"></i></span>
						       <input type="number" class="form-control" name="age" id="age" placeholder="<spring:message code="register.age"></spring:message>" min=1 max=150>
						  </div>
						</div>
						</div>
						
							<div class="form-group">
						<div class="cols-sm-6 col-md-4 col-md-offset-4">
						  <div class="input-group">
						       <span class="input-group-addon"><i class="fa fa-briefcase fa" aria-hidden="true"></i></span>
						       <input type="text" class="form-control" name="job" id="job" placeholder="<spring:message code="register.job"></spring:message>" >
						  </div>
						</div>
						</div>
						
						<div class="form-group">
						<button type="submit" class="btn btn-primary btn-lg btn-lg login-button" ><spring:message code="signup.text"></spring:message></button>
						</div>
						
						<div class="login-register">
						<a href=<c:url value="/"/>><spring:message code="turnback.text"></spring:message></a>
						</div>
						
						<div>
					
						
						<c:if test="${not empty errs}">
						<c:forEach items="${errs}" var="err">
						<p style="color: red;">${err}</p>
						</c:forEach>
						</c:if>
						</div>
						</form>
						</div>
					</section>
				</div>
			</div>


      <script src="${jqueryMinJS}"></script>
		<script src="${jqueryMigrateJS}"></script>
		<script src="${bootstrapMinJS}"></script>
		<script src="${jqueryActualJS}"></script>
		<script src="${jqueryScrollJS}"></script>
		<script src="${bootstrapMinJS}"></script>
		<script src="${bootstrapJS}"></script>
		<script src="${npmJS}"></script>
		<script src="${scriptJS}"></script>
	</body>
</html>
