<%@page import="org.springframework.security.crypto.codec.Base64"%>
<%@page import="com.boodex.domain.Book"%>
<%@page import="com.boodex.dao.BookDao"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:set var="pathUrl" value="${pageContext.request.contextPath}/"/>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>

<title>Books</title>
<script src="https://code.jquery.com/jquery-3.1.1.js" integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA=" crossorigin="anonymous"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.css"/>
 
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.js"></script>

<spring:url value="/resources/assets/images/book_default.png" var="bookDefault"></spring:url>
<spring:url value="/resources/assets/images/arrow_down.png" var="arrowImage"></spring:url>

<meta name="viewport" content="width=device-width, initial-scale = 1.0, maximum-scale=1.0, user-scalable=no"/>
		

		<!-- google fonts -->
		<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=PT+Sans'>
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Droid+Serif:regular,bold"/>
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Alegreya+Sans:regular,italic,bold,bolditalic"/>
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Nixie+One:regular,italic,bold,bolditalic"/>
		<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Alegreya+SC:regular,italic,bold,bolditalic"/>
		
	
 
		<spring:url value="/resources/assets/js/jquery-2.1.3.min.js" var="jqueryMinJS"></spring:url>
<spring:url value="/resources/assets/js/bootstrap.min.js" var="bootstrapMinJS"></spring:url>
<spring:url value="/resources/assets/js/jquery-migrate-1.2.1.min.js" var="jqueryMigrateJS"></spring:url>
<spring:url value="/resources/assets/js/jquery.actual.min.js" var="jqueryActualJS"></spring:url>
<spring:url value="/resources/assets/js/jquery.scrollTo.min.js" var="jqueryScrollJS"></spring:url>
<spring:url value="/resources/assets/js/bootstrap.js" var="bootstrapJS"></spring:url>
<spring:url value="/resources/assets/js/npm.js" var="npmJS"></spring:url>
<spring:url value="/resources/assets/js/script.js" var="scriptJS"></spring:url>

<spring:url value="/resources/assets/css/bootstrap.min.css" var="bootstrapCSS"></spring:url>
<spring:url value="/resources/assets/css/font-awesome.min.css" var="fontCSS"></spring:url>
<spring:url value="/resources/assets/css/style.css" var="styleCSS"></spring:url>

<!-- css -->
		<link href="${bootstrapCSS}" rel="stylesheet">
		<link href="${fontCSS}" rel="stylesheet" >
		<link href="${styleCSS}" rel="stylesheet" media="screen"/>
		
	</head>
<body>
 
 <div style="margin-left: 15px; margin-top: 20px;"><h4>Hoşgeldiniz, Sayın ${author.authorNameSurname}</h4></div>
 <form style="margin-left: 15px;"action="logout" method="POST"><button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-sign-out fa"></i>&nbsp;Çıkış</button></form>
 <div class="container" style="margin-top: 20px; margin-bottom:10px; text-align: center;">
  <form style="margin-left: 40px; margin-right: 40px;" method="POST" action="createBook" id="formNew" enctype="multipart/form-data">
  <div class="form-group">
<label for="bookName">Kitap İsmi</label>
    <input type="text" class="form-control" name="bookName" id="bookName" placeholder="Kitabının Adı" required>
  </div>
  <div class="form-group">
    <label for="bookType">Tür</label>
    <input type="text" class="form-control" name="bookType" id="bookType" placeholder="Kitabının Türü" required>
  </div>
  
  <div class="form-group">
    <label for="image">Fotoğrafı: </label>
 <label class="custom-file">
  <input type="file" name="bookImage" id="file" class="custom-file-input" required>
  <span class="custom-file-control"></span>
</label>
  </div>
  <div class="form-group">
    <label for="image">PDF Dosyası: </label>
 <label class="custom-file">
  <input type="file" name="bookPdf" id="file" class="custom-file-input" required>
  <span class="custom-file-control"></span>
</label>
  </div>
    
  <button type="submit" name="loginAuthor" class="btn btn-primary"><i class="fa fa-plus-circle fa"></i>&nbsp;Create</button>
</form>
 </div>
 <table id="example" class="display" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Kitap Fotoğrafı</th>
                <th>Kitap İsmi</th>
                <th>Türü</th>
                <th><i class="fa fa-download fa"></i>&nbsp;İndirilme Sayısı</th>
                <th></th>
            </tr>
        </thead>
        <tfoot>
            <tr>
                <th>Kitap Fotoğrafı</th>
                <th>Kitap İsmi</th>
                <th>Türü</th>
                <th><i class="fa fa-download fa"></i>&nbsp;İndirilme Sayısı</th>
                <th></th>
            </tr>
        </tfoot>
        <tbody>
        <c:if test="${not empty author.books}">
        <c:forEach items="${author.books}" var="book">
            <tr>
                <td>
                <%
                 
                Book book=(Book)pageContext.getAttribute("book");
                byte[] encodeBase64 = Base64.encode(book.getImageData());
                String base64Encoded = new String(encodeBase64, "UTF-8");
        
                %>
                 <img src="data:image/jpeg;base64,<%=base64Encoded%>" width="100px" height="100px"></img>
            </td>
                <td>${book.name}</td>
                <td>${book.type}</td>
                <td>${book.downloadCount}</td>
                <td><a href=<c:url value="/contents?bookId=${book.id}"></c:url> class="btn btn-success btn-sm"><i class="fa fa-hand-o-right fa"></i>&nbsp;Git</a>&nbsp;<a href=<c:url value="/delete?id=${book.id}"></c:url> class="btn btn-danger btn-sm"><i class="fa fa-trash-o fa"></i>&nbsp;Sil</a></td>
            </tr>
            </c:forEach>
            </c:if>
        </tbody>
    </table>
  
  
 
   
   
<script src="${jqueryMinJS}"></script>
		<script src="${jqueryMigrateJS}"></script>
		<script src="${bootstrapMinJS}"></script>
		<script src="${jqueryActualJS}"></script>
		<script src="${jqueryScrollJS}"></script>
		<script src="${bootstrapMinJS}"></script>
		<script src="${bootstrapJS}"></script>
		<script src="${npmJS}"></script>
		<script src="${scriptJS}"></script>
		
	
		<script type="text/javascript">
		
		$(document).ready(function() {
			
    $('#example').DataTable();
   
} );  </script>

</body>
</html>