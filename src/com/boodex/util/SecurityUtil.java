package com.boodex.util;


import java.security.SecureRandom;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;


public class SecurityUtil {

	private static SecurityUtil instance=null;
	
	private static final String keyValue="1357642812341234";
	
	private byte[] key;
	
	public static synchronized SecurityUtil getInstance(){
		if(instance==null){
			instance=new SecurityUtil();
		}
		return instance;
		
	}
	
	private SecurityUtil(){
		this.key=keyValue.getBytes();
		
	}
	
	public String decryptAES(String toDecrypt) throws Exception{
		
		SecretKeySpec secretKeySpec=new SecretKeySpec(key,"AES");
		
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.DECRYPT_MODE, secretKeySpec);
		
		byte[] decrpted=Base64.getDecoder().decode(toDecrypt);
		byte[] decryptedValue=cipher.doFinal(decrpted);
		
		return new String(decryptedValue,"UTF-8");
	}
	
	
public String encryptedAES(String toEncrypt) throws Exception{
		
		SecretKeySpec secretKeySpec=new SecretKeySpec(key,"AES");
		
		Cipher cipher = Cipher.getInstance("AES");
		cipher.init(Cipher.ENCRYPT_MODE, secretKeySpec);
		
		byte[] encrypted=cipher.doFinal(toEncrypt.getBytes("UTF-8"));
		String encryptedValue=Base64.getEncoder().encodeToString(encrypted);
		
		return encryptedValue;
	}
	

static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-*!";
static SecureRandom rnd = new SecureRandom();

public String randomString( int len ){
   StringBuilder sb = new StringBuilder( len );
   for( int i = 0; i < len; i++ ) 
      sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
   return sb.toString();
}
	
	
}
