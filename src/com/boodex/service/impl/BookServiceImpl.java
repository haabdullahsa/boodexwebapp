package com.boodex.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.boodex.dao.BookDao;
import com.boodex.domain.Book;
import com.boodex.domain.Content;
import com.boodex.service.BookService;

@Service
@Transactional
public class BookServiceImpl implements BookService {

	@Autowired 
	private BookDao bookDaoImpl;
	
	@Override
	public Boolean createContent(Content content, int bookId) {
		// TODO Auto-generated method stub
		return bookDaoImpl.createContent(content, bookId);
	}

	@Override
	public Boolean updateContent(int contentId,Content content) {
		// TODO Auto-generated method stub
		return bookDaoImpl.updateContent(contentId, content);
	}

	@Override
	public Boolean deleteContent(int contentId) {
		// TODO Auto-generated method stub
		return bookDaoImpl.deleteContent(contentId);
	}

	@Override
	public List<Content> getContents(int bookId) {
		// TODO Auto-generated method stub
		return bookDaoImpl.getContents(bookId);
	}

	@Override
	public Content getContent(int contentId) {
		// TODO Auto-generated method stub
		return bookDaoImpl.getContent(contentId);
	}
	
	@Override
	public List<Content> getBooksBySearch(String searchKey, String type, String authorName, int filterNo) {
		// TODO Auto-generated method stub
		return bookDaoImpl.getBooksBySearch(searchKey, type, authorName, filterNo);
	}

	@Override
	public Book getBook(int bookId) {
		// TODO Auto-generated method stub
		return bookDaoImpl.getBook(bookId);
	}

}
