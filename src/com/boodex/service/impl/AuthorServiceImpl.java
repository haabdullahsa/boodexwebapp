package com.boodex.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.boodex.dao.AuthorDao;
import com.boodex.domain.Author;
import com.boodex.domain.Book;
import com.boodex.service.AuthorService;

@Service
@Transactional
public class AuthorServiceImpl implements AuthorService {

	@Autowired
	private AuthorDao authorDaoImpl;
	
	@Override
	public Author loginAuthor(String email, String password) {
		// TODO Auto-generated method stub
		return authorDaoImpl.loginAuthor(email, password);
	}

	@Override
	public Boolean registerAuthor(byte[] image,String nameSurname,String email, String password) {
		// TODO Auto-generated method stub
		return authorDaoImpl.registerAuthor(image,nameSurname, email, password);
	}

	@Override
	public Boolean addBook(Book book, int authorId) {
		// TODO Auto-generated method stub
		return authorDaoImpl.addBook(book, authorId);
	}

	@Override
	public Boolean deleteBook(int bookId) {
		// TODO Auto-generated method stub
		return authorDaoImpl.deleteBook(bookId);
	}

	@Override
	public List<Book> getBooks(int authorId) {
		// TODO Auto-generated method stub
		return authorDaoImpl.getBooks(authorId);
	}
	
	@Override
	public List<Author> getAuthors() {
		// TODO Auto-generated method stub
		return authorDaoImpl.getAuthors();
	}

}
