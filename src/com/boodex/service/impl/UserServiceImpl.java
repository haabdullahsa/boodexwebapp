package com.boodex.service.impl;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.boodex.dao.UserDao;
import com.boodex.domain.User;
import com.boodex.service.UserService;

@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDaoImpl;
	
	@Override
	public User loginUser(String email, String password) {
		// TODO Auto-generated method stub
		return userDaoImpl.loginUser(email, password);
	}

	@Override
	public Boolean registerUser(String nameSurname, String email, String password, String gender, String age,
			String job) {
		// TODO Auto-generated method stub
		return userDaoImpl.registerUser(nameSurname, email, password, gender, age, job);
	}

	@Override
	public Boolean addDownloadBook(int userId, int bookId) {
		// TODO Auto-generated method stub
		return userDaoImpl.addDownloadBook(userId, bookId);
	}

	@Override
	public Boolean addSearchKey(int userId, String key) {
		// TODO Auto-generated method stub
		return userDaoImpl.addSearchKey(userId, key);
	}

}
