package com.boodex.service;

import java.util.List;

import com.boodex.domain.Search;
import com.boodex.domain.User;

public interface UserService {

	public User loginUser(String email,String password);
	public Boolean registerUser(String nameSurname,String email,String password,String gender,
			String age,String job);
	
	public Boolean addDownloadBook(int userId,int bookId);
	public Boolean addSearchKey(int userId,String key);
}
