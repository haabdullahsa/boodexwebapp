package com.boodex.service;

import java.util.List;

import com.boodex.domain.Book;
import com.boodex.domain.Content;

public interface BookService {

	public Book getBook(int bookId);
	public Boolean createContent(Content content,int bookId);
	public Boolean updateContent(int contentId,Content content);
	public Boolean deleteContent(int contentId);
	public List<Content> getContents(int bookId);
	public Content getContent(int contentId);
	public List<Content> getBooksBySearch(String searchKey,String type,String authorName,int filterNo);
	
}
