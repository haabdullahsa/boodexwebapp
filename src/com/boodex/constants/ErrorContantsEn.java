package com.boodex.constants;

public class ErrorContantsEn {
       
	public static final String EMAIL_NULL_ERROR = "Email is required.";
	public static final String NAME_NULL_ERROR = "Name and Surname is required.";
	public static final String PASSWORD_IS_NOT_VALID = "Password should be at least 4 characters";
	public static final String PASSWORD_IS_NOT_CONFIRMED="Password should be same";
	public static final String REGISTER_SUCCESS="You Are Our Author Anymore! But, after we review your profile, your account is activated.";
	public static final String IS_NOT_UPLOADED="Your Image is not uploaded.";
	public static final String PHOTO_IS_EMPTY="Photo shouldn't be empty.";
}
