package com.boodex.constants;

public class ErrorContantsTr {
       
	public static final String EMAIL_NULL_ERROR = "Email adresi bo� b�rak�lamaz.";
	public static final String NAME_NULL_ERROR = "�sim alan� bo� b�rak�lamaz.";
	public static final String PASSWORD_IS_NOT_VALID = "Parola en az 4 haneden olu�mal�d�r.";
	public static final String PASSWORD_IS_NOT_CONFIRMED="Parolan�z �nceki ile ayn� de�il.";
	public static final String REGISTER_SUCCESS="Art�k Yazar�m�zs�n! Gerekli incelemelerden sonra aktif edilecektir.";
	public static final String IS_NOT_UPLOADED="Foto�raf y�klenemedi.";
	public static final String PHOTO_IS_EMPTY="Foto�raf alan� bo� olamaz.";
	
}
