package com.boodex.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="CONTENT")
public class Content implements Serializable {

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getSubjectName() {
		return subjectName;
	}

	public void setSubjectName(String subjectName) {
		this.subjectName = subjectName;
	}

	public String getMinPageNumber() {
		return minPageNumber;
	}

	public void setMinPageNumber(String minPageNumber) {
		this.minPageNumber = minPageNumber;
	}

	public String getMaxPageNumber() {
		return maxPageNumber;
	}

	public void setMaxPageNumber(String maxPageNumber) {
		this.maxPageNumber = maxPageNumber;
	}

	public String getPreviewCount() {
		return previewCount;
	}

	public void setPreviewCount(String previewCount) {
		this.previewCount = previewCount;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="subject",nullable=false)
	private String subjectName;
	
	@Column(name="minPageNumber",nullable=false)
	private String minPageNumber;
	
	@Column(name="maxPageNumber",nullable=false)
	private String maxPageNumber;
	
	@Column(name="previewCount")
	private String previewCount;
	
	@ManyToOne
	private Book book;
}
