package com.boodex.domain;

import java.io.Serializable;
import java.sql.Blob;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.OneToMany;
import javax.persistence.Table;


@Entity
@Table(name="AUTHOR")
public class Author implements Serializable {


	public byte[] getProfileImage() {
		return profileImage;
	}

	public void setProfileImage(byte[] profileImage) {
		this.profileImage = profileImage;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAuthorNameSurname() {
		return authorNameSurname;
	}

	public void setAuthorNameSurname(String authorNameSurname) {
		this.authorNameSurname = authorNameSurname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isActive() {
		return isActive;
	}

	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	public List<Book> getBooks() {
		return books;
	}

	public void setBooks(List<Book> books) {
		this.books = books;
	}

	@Id
    @Column(name="id")
    @GeneratedValue(strategy = GenerationType.IDENTITY )
    private Integer id;
    
    @Column(name="authorNameSurname",nullable=false)
    private String authorNameSurname;
    
    @Column(name="email",unique=true,nullable=false)
    private String email;
    
    @Column(name="password",nullable=false)
    private String password;
   

	@Lob
	@Column(name="profileImage", nullable=false, columnDefinition="mediumblob")
    private byte[] profileImage;
    
    @Column(name="isActive")
    private boolean isActive;
    
    @OneToMany(fetch = FetchType.EAGER,mappedBy="author",cascade=CascadeType.ALL)
	 private List<Book> books;
    
    
    
}
