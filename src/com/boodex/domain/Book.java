package com.boodex.domain;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="BOOK")
public class Book implements Serializable {

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public byte[] getImageData() {
		return imageData;
	}

	public void setImageData(byte[] imageData) {
		this.imageData = imageData;
	}

	public byte[] getBookFile() {
		return bookFile;
	}

	public void setBookFile(byte[] bookFile) {
		this.bookFile = bookFile;
	}

	public Integer getDownloadCount() {
		return downloadCount;
	}

	public void setDownloadCount(Integer downloadCount) {
		this.downloadCount = downloadCount;
	}

	public Author getAuthor() {
		return author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public List<Content> getContents() {
		return contents;
	}

	public void setContents(List<Content> contents) {
		this.contents = contents;
	}

	public Set<User> getUsers() {
		return users;
	}

	public void setUsers(Set<User> users) {
		this.users = users;
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="bookId")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="name",unique = true,nullable=false)
	private String name;
	
	@Column(name="type",nullable=false)
	private String type;
	
	@Lob
	@Column(name="imageData", nullable=false, columnDefinition="mediumblob")
	private byte[] imageData;
	
	@Lob
	@Column(name="bookFile",nullable=false,columnDefinition = "longblob")
	private byte[] bookFile;
	
	@Column(name="downloadCount")
	private Integer downloadCount;
	
	@Column(name="createdDate")
	private Date createdDate;
	
	@ManyToOne
	private Author author;
	
	@OneToMany(fetch=FetchType.EAGER,mappedBy="book",cascade=CascadeType.ALL)
	 private List<Content> contents;
	
	@ManyToMany(fetch=FetchType.EAGER,mappedBy="books")
	private Set<User> users=new HashSet<User>();
	

	
}
