package com.boodex.dao;

import java.sql.Blob;
import java.util.List;

import com.boodex.domain.Author;
import com.boodex.domain.Book;
import com.boodex.domain.Content;

public interface AuthorDao {

	public Author loginAuthor(String email,String password);
	public Boolean registerAuthor(byte[] image,String nameSurname,String email,String password);
	public Boolean addBook(Book book,int authorId);
	public Boolean deleteBook(int bookId);
	public List<Book> getBooks(int authorId);
	public List<Author> getAuthors();
	
	
}
