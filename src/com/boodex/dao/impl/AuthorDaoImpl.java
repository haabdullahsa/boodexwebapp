package com.boodex.dao.impl;

import java.sql.Blob;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.management.Query;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.boodex.dao.AuthorDao;
import com.boodex.domain.Author;
import com.boodex.domain.Book;
import com.boodex.domain.User;
import com.boodex.util.SecurityUtil;

@Repository
public class AuthorDaoImpl implements AuthorDao {

	@Autowired
	SessionFactory sessionFactory;
	
	
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	
	@Override
	public Author loginAuthor(String email, String password) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		String sql = "SELECT * FROM Author WHERE email = :email AND password = :password";
		SQLQuery query = session.createSQLQuery(sql);
		query.addEntity(Author.class);
		query.setParameter("email", email);
		try {
			query.setParameter("password", SecurityUtil.getInstance().encryptedAES(password));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<Author> results = (List<Author>)query.list();
		
		if(results.isEmpty()){
			return null;
		}
		return results.get(0);
	}

	@Override
	public Boolean registerAuthor(byte[] image,String nameSurname,String email, String password) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		Author author=new Author();
		author.setAuthorNameSurname(nameSurname);
		try {
			author.setPassword(SecurityUtil.getInstance().encryptedAES(password));
			author.setEmail(email);
		    author.setProfileImage(image);
			session.save(author);
			return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}

	@Override
	public Boolean addBook(Book book, int authorId) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		Date date=new Date();
		Author author=(Author)session.get(Author.class, authorId);
		book.setAuthor(author);
		author.getBooks().add(book);
		book.setCreatedDate(date);
		session.persist(book);
		session.update(author);
		return true;
	}

	@Override
	public Boolean deleteBook(int bookId) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		Book book=(Book)session.get(Book.class, bookId);
		book.getAuthor().getBooks().clear();
		book.getUsers().clear();
		session.delete(book);
		
		session.flush(); 
		
		return true;
	}

	@Override
	public List<Book> getBooks(int authorId) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		Author author=(Author)session.get(Author.class, authorId);
		return author.getBooks();
	}
	
	@Override
	public List<Author> getAuthors() {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		org.hibernate.Query query=session.createQuery("from Author");
		List<Author> authors=(List<Author>)query.list();
		return authors;
	}

}
