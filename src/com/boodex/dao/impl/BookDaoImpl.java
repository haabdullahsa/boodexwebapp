package com.boodex.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;

import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.boodex.dao.BookDao;
import com.boodex.domain.Book;
import com.boodex.domain.Content;


@Repository
public class BookDaoImpl implements BookDao {

	@Autowired
	SessionFactory sessionFactory;
	
	
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	public Boolean createContent(Content content, int bookId) {
	   Session session=sessionFactory.getCurrentSession();
	   Book book=(Book)session.get(Book.class,bookId);
	   content.setBook(book);
	   book.getContents().add(content);
	   session.save(content);
		return true;
	}

	@Override
	public Boolean updateContent(int contentId,Content updatedContent) {
		// TODO Auto-generated method stub
		  Session session=sessionFactory.getCurrentSession();
		   Content content=(Content)session.get(Content.class,contentId);
		   if(updatedContent!=null){
			   content.setMaxPageNumber(updatedContent.getMaxPageNumber());
			   content.setMinPageNumber(updatedContent.getMinPageNumber());
			   content.setSubjectName(updatedContent.getSubjectName());
			   session.update(content);
			   return true;
		   }
		  
		return false;
	}

	@Override
	public Boolean deleteContent(int contentId) {
		// TODO Auto-generated method stub
		 Session session=sessionFactory.getCurrentSession();
		   Content content=(Content)session.get(Content.class,contentId);
		   content.getBook().getContents().clear();
		   content.getBook().getUsers().clear();
		   session.delete(content);
		return true;
	}

	@Override
	public List<Content> getContents(int bookId) {
		 Session session=sessionFactory.getCurrentSession();
		 Book book=(Book)session.get(Book.class, bookId);
		// TODO Auto-generated method stub
		return book.getContents();
	}

	@Override
	public Content getContent(int contentId) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		Content content=(Content)session.get(Content.class, contentId);
		return content;
	}

	@Override
	public List<Content> getBooksBySearch(String searchKey, String type, String authorName, int filterNo) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		String sql="from Content";
		if(filterNo == 0){
			if(!searchKey.equals("")){
				sql="from Content c where c.subjectName like '%"+searchKey+"%'";
				if(!type.equals("")){
					sql+=" AND c.book.type like '%"+type+"%'";
				}
				if(!authorName.equals("")){
					sql+=" AND c.book.author.authorNameSurname like '%"+authorName+"%'";
				}	
			}
		}else if(filterNo == 1){
			if(!searchKey.equals("")){
				sql="from Content c where c.subjectName like '%"+searchKey+"%'";
				if(!type.equals("") ){
					sql+=" AND c.book.type like '%"+type+"%'";
				}
				if(!authorName.equals("") ){
					sql+=" AND c.book.author.authorNameSurname like '%"+authorName+"%'";
				}
			}
			 sql+=" order by c.book.downloadCount desc";
		}else if(filterNo == 2){
			if(!searchKey.equals("")){
				sql="from Content c where c.subjectName like '%"+searchKey+"%'";
				if(!type.equals("")){
					sql+=" AND c.book.type like '%"+type+"%'";
				}
				if(authorName.equals("")){
					sql+=" AND c.book.author.authorNameSurname like '%"+authorName+"%'";
				}
				 
			}
			sql+=" order by c.book.createdDate desc";
		}
		
		Query query=session.createQuery(sql);
		List<Content> books= (List<Content>)query.list();
		
		return books;
	}

	@Override
	public Book getBook(int bookId) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		Book book=(Book)session.get(Book.class, bookId);
		
		return book;
	}
}
