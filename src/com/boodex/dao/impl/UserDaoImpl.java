package com.boodex.dao.impl;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.boodex.constants.ErrorContantsTr;
import com.boodex.dao.UserDao;
import com.boodex.domain.Book;
import com.boodex.domain.Search;
import com.boodex.domain.User;
import com.boodex.util.SecurityUtil;

@Repository
public class UserDaoImpl implements UserDao {

	
	@Autowired
	SessionFactory sessionFactory;
	
	
	
	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}
	
	@Override
	public User loginUser(String email, String password) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		String sql = "SELECT * FROM User WHERE email = :email AND password = :password";
		SQLQuery query = session.createSQLQuery(sql);
		query.addEntity(User.class);
		query.setParameter("email", email);
		try {
			query.setParameter("password", SecurityUtil.getInstance().encryptedAES(password));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<User> results = (List<User>)query.list();
		if(results.isEmpty()){
			return null;
		}
		return results.get(0);
	}

	@Override
	public Boolean registerUser(String nameSurname, String email, String password, String gender, String age,
			String job) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		User user=new User();
		if(password == null || password.length()<4){
			return false;
		}else{
			user.setNameSurname(nameSurname);
			try {
				user.setPassword(SecurityUtil.getInstance().encryptedAES(password));
				user.setGender(gender);
				user.setEmail(email);
				user.setAge(age);
				user.setJob(job);
				session.persist(user);
				return true;
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		
		return false;
		
		
	}

	@Override
	public Boolean addDownloadBook(int userId, int bookId) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		User user=(User)session.get(User.class, userId);
		Book book=(Book)session.get(Book.class, bookId);
		user.getBooks().add(book);
		session.update(user);
		return true;
	}

	@Override
	public Boolean addSearchKey(int userId, String key) {
		// TODO Auto-generated method stub
		Session session=sessionFactory.getCurrentSession();
		User user=(User)session.get(User.class, userId);
		Search search=new Search();
		search.setUser(user);
		search.setSearchKey(key);
		user.getSearches().add(search);
		session.save(search);
		return null;
	}

}
