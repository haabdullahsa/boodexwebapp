package com.boodex.handler;

import java.io.InputStream;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.boodex.domain.Author;
import com.boodex.domain.User;
import com.boodex.service.AuthorService;
import com.boodex.service.UserService;
import com.boodex.util.SecurityUtil;

public class SessionInterceptor extends HandlerInterceptorAdapter {
	
	@Autowired
	private UserService userServiceImpl;
	
	@Autowired
	private AuthorService authorServiceImpl;

	@Override
	public void afterCompletion(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, Exception arg3)
			throws Exception {
		// TODO Auto-generated method stub
		
	}


	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {

		// TODO Auto-generated method stub
		
		if(request.getMethod().equals("POST") && request.getRequestURI().endsWith("login")){
			String email=request.getParameter("email");
			String password=request.getParameter("password");
			User user=userServiceImpl.loginUser(email, password);
			
			if(user!=null){
				HttpSession session=request.getSession();
				session.setAttribute("user", user);
				session.setMaxInactiveInterval(30*60);
				Cookie userName = new Cookie("user", user.getNameSurname());
				userName.setMaxAge(30*60);
				response.addCookie(userName);
				
			}
			
			
			
		}
		
		if(request.getMethod().equals("POST") && request.getRequestURI().endsWith("logout")){
			HttpSession session=request.getSession(false);
			session.invalidate();
		}
		
		
		
		if(request.getMethod().equals("POST") && request.getRequestURI().endsWith("authorBooks")){
			String email=request.getParameter("email");
			String password=request.getParameter("password");
			if(email==null || password==null){
				Author author=(Author)request.getSession().getAttribute("author");
				email=author.getEmail();
				password=SecurityUtil.getInstance().decryptAES(author.getPassword());
			}
			Author author=authorServiceImpl.loginAuthor(email, password);
			
			if(author!=null){
				HttpSession session=request.getSession();
				session.setAttribute("author", author);
				session.setMaxInactiveInterval(30*60);
				Cookie authorName = new Cookie("authorName", author.getAuthorNameSurname());
				authorName.setMaxAge(30*60);
				response.addCookie(authorName);
			}
			
		
		}
		
		
		
		
		
		return true;
	
	}
	
		
	
	
	
}
