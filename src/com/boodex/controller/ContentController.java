package com.boodex.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.boodex.domain.Author;
import com.boodex.domain.Content;
import com.boodex.service.AuthorService;
import com.boodex.service.BookService;
import com.boodex.service.impl.BookServiceImpl;

@Controller
public class ContentController {

	@Autowired
	BookService bookServiceImpl;
	
	@RequestMapping(value="/createContent{id}",method = RequestMethod.POST)
	public String createContent(@RequestParam("id")int id,HttpServletRequest req,ModelMap map){
		HttpSession session=req.getSession();
		Content content=new Content();
		content.setMaxPageNumber(req.getParameter("lastPageNo"));
		content.setMinPageNumber(req.getParameter("firstPageNo"));
		content.setSubjectName(req.getParameter("content"));
		
		if(session!=null){
			Author author=(Author)session.getAttribute("author");
			if(author==null){
				return "redirect:/author";
			}
			try{
				Boolean isCreate = bookServiceImpl.createContent(content,id);
				java.util.List<Content> bookContents = bookServiceImpl.getContents(id);
	    		map.addAttribute("contents",bookContents);
	    		map.addAttribute("book",bookContents.get(0).getBook());
				
			}catch(Exception e){
				return "redirect:/author";
			}
				
		}
		
		return "bookContentPage";
	}
	
	@RequestMapping(value="/updateContent/{bookId}/update{id}",method = RequestMethod.POST)
	public String updateContent(@RequestParam("id")int id,@PathVariable("bookId")int bookId,HttpServletRequest req,ModelMap map){
		HttpSession session=req.getSession();
		Content content=new Content();
		content.setMaxPageNumber(req.getParameter("lastPageNo"));
		content.setMinPageNumber(req.getParameter("firstPageNo"));
		content.setSubjectName(req.getParameter("content"));
		
		if(session!=null){
			Author author=(Author)session.getAttribute("author");
			if(author==null){
				return "redirect:/author";
			}
			try{
				Boolean isCreate = bookServiceImpl.updateContent(id, content);
				java.util.List<Content> bookContents = bookServiceImpl.getContents(bookId);
	    		map.addAttribute("contents",bookContents);
	    		map.addAttribute("book",bookContents.get(0).getBook());
				
			}catch(Exception e){
				return "redirect:/author";
			}
				
		}
		
		return "bookContentPage";
	}
	
	
	
	@RequestMapping(value="/deleteContent/{bookId}/delete{id}",method = RequestMethod.GET)
	public String deleteContent(@PathVariable("bookId")int bookId,@RequestParam("id")int id,HttpServletRequest req,ModelMap map){
		HttpSession session=req.getSession();
		
		if(session!=null){
			Author author=(Author)session.getAttribute("author");
			if(author==null){
				return "redirect:/author";
			}
			try{
				Boolean isDelete = bookServiceImpl.deleteContent(id);
				java.util.List<Content> bookContents = bookServiceImpl.getContents(bookId);
	    		map.addAttribute("contents",bookContents);
	    		map.addAttribute("book",bookContents.get(0).getBook());
				
			}catch(Exception e){
				return "redirect:/author";
			}
				
		}
		
		return "bookContentPage";
	}
	
}
