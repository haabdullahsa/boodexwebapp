package com.boodex.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.boodex.domain.Author;
import com.boodex.domain.Book;
import com.boodex.domain.Content;
import com.boodex.domain.User;
import com.boodex.service.AuthorService;
import com.boodex.service.BookService;
import com.boodex.service.UserService;

import antlr.collections.List;

@Controller
public class SearchController {
	
	@Autowired
	BookService bookServiceImpl;
	
	@Autowired
	AuthorService authorServiceImpl;

	@Autowired
	UserService userServiceImpl;
	
	
	@RequestMapping(value="/getListByFilter",method=RequestMethod.POST)
	public String getSearchListFilter(HttpServletRequest req,ModelMap map){
		String authorName=req.getParameter("authorName");
		String type=req.getParameter("type");
		String key=req.getParameter("key");
		int filterNo=Integer.parseInt(req.getParameter("filter"));
		
		try{
			java.util.List<Content> books=bookServiceImpl.getBooksBySearch(key, type, authorName, filterNo);
			map.addAttribute("books",books);
			HttpSession session=req.getSession();
			if(session!=null){
				 User user=(User)req.getSession().getAttribute("user");
				    if(user != null){
				    	userServiceImpl.addSearchKey(user.getId(), key);
				    }
			}
			 java.util.List<Author> authors=authorServiceImpl.getAuthors();
				map.addAttribute("authors",authors);
		}catch(Exception e){
			map.addAttribute("books",null);	
			map.addAttribute("authors",null);
		}
		
		
		
		return "index";
	}
	
	@RequestMapping("/viewPdf{id}")
	public String viewPdf(@RequestParam("id")int id,ModelMap map){
		try{
			Book book=bookServiceImpl.getBook(id);
			map.addAttribute("book",book);
		} catch (Exception e) {
			// TODO: handle exception
		}
		
		return "pdfView";
	}
	
}
