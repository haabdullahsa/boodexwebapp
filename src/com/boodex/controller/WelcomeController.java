package com.boodex.controller;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.util.ArrayList;
import java.util.Locale;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import org.apache.commons.io.IOUtils;
import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import com.boodex.constants.ErrorContantsEn;
import com.boodex.constants.ErrorContantsTr;
import com.boodex.domain.Author;
import com.boodex.domain.Book;
import com.boodex.domain.Content;
import com.boodex.domain.User;
import com.boodex.service.AuthorService;
import com.boodex.service.BookService;
import com.boodex.service.UserService;
import com.mysql.jdbc.log.Log;

@Controller
public class WelcomeController {

	@Autowired
	UserService userServiceImpl;
	
	@Autowired
	AuthorService authorServiceImpl;
	
	@Autowired
	BookService bookServiceImpl;
	
	@RequestMapping(value="/")
	public String getIndex(HttpServletRequest request,ModelMap map){
		HttpSession session=request.getSession();
		if(session!=null){
			 User user=(User)request.getSession().getAttribute("user");
			    if(user == null){
			    	 map.addAttribute("user",null);
			    }else{
			    	  map.addAttribute("user",user);
			    }
			  
		}
		
		try{
			java.util.List<Content> books=bookServiceImpl.getBooksBySearch("", "", "", 0);
			map.addAttribute("books",books);
			java.util.List<Author> authors=authorServiceImpl.getAuthors();
			map.addAttribute("authors",authors);
		}catch(Exception e){
			map.addAttribute("books",null);	
			map.addAttribute("authors",null);	
		}
	
		return "index";
	}
	
	@RequestMapping(value="/login",method=RequestMethod.POST)
	public String getLogin(Model map){
		
		return "redirect:/";
	}
	
	@RequestMapping(value="/logout",method=RequestMethod.POST)
	public String getLogout(Model map){
	
		return "redirect:/";
	}
	
	@RequestMapping(value="/register",method=RequestMethod.POST)
	public String getPostRegisterMember(HttpServletRequest request,ModelMap map){
		
		String nameSurname=request.getParameter("name");
		String email=request.getParameter("email");
		String password=request.getParameter("password");
		String gender=request.getParameter("genders");
		String age=request.getParameter("age");
		String job=request.getParameter("job");
		
		String success=null;
		
		ArrayList<String> errs=new ArrayList<String>();
		if( nameSurname == null || nameSurname.trim().equals("") ){
			if(!LocaleContextHolder.getLocale().toString().equals("en"))
			errs.add(ErrorContantsTr.NAME_NULL_ERROR);
			else
				errs.add(ErrorContantsEn.NAME_NULL_ERROR);
		} if(email == null || email.trim().equals("")){
			if(!LocaleContextHolder.getLocale().toString().equals("en"))
				errs.add(ErrorContantsTr.EMAIL_NULL_ERROR);
				else
					errs.add(ErrorContantsEn.EMAIL_NULL_ERROR);
			
		} if(password == null || password.length()<4){
			if(LocaleContextHolder.getLocale().getLanguage() != "en")
				errs.add(ErrorContantsTr.PASSWORD_IS_NOT_VALID);
				else
			errs.add(ErrorContantsEn.PASSWORD_IS_NOT_VALID);
		}	
		
	    Boolean isRegister=userServiceImpl.registerUser(nameSurname, email, password, gender, age, job);
	    if(!isRegister){
	    	map.addAttribute("errs",errs);
	    }else{
	    	success="Art�k �yemizsin!";
	    }
	    map.addAttribute("success",success);
		return "post";
	
	}
	
	@RequestMapping("/about")
	public String getAboutPage(){
		return "about";
	}
	
	@RequestMapping(value="/register",method=RequestMethod.GET)
	public String getRegisterPage(){
		return "post";
	}
	
	@RequestMapping("/author")
	public String getAuthorPage(HttpServletRequest request,ModelMap map){
		HttpSession session=request.getSession();
		if(session!=null){
			 Author author=(Author)request.getSession().getAttribute("author");
			    if(author != null){
			    	return "redirect:/authorBooks";
			    }
			  
		}
		return "authorPage";
	}
	
	@RequestMapping(value="/authorRegister", method = RequestMethod.POST)
	public String getAuthorBookFromRegister(@RequestParam("profileImage") MultipartFile file,ModelMap map,HttpServletRequest request){
		String email=request.getParameter("reg_email");
		String nameSurname=request.getParameter("reg_nameSurname");
		String passwordConfirm=request.getParameter("reg_password_confirm");
		String password=request.getParameter("reg_password");
		
		
		ArrayList<String> errs=new ArrayList<String>();
	
		if(file.isEmpty()){
			if(!LocaleContextHolder.getLocale().toString().equals("en"))
				errs.add(ErrorContantsTr.PHOTO_IS_EMPTY);
				else
					errs.add(ErrorContantsEn.PHOTO_IS_EMPTY);
		}else{
			if(password.equals(passwordConfirm)){
				  Boolean isRegister;
				try {
					isRegister = authorServiceImpl.registerAuthor( file.getBytes(),nameSurname, email, password);
					 if(isRegister){
						  
						  if(!LocaleContextHolder.getLocale().toString().equals("en"))
								errs.add(ErrorContantsTr.REGISTER_SUCCESS);
								else
									errs.add(ErrorContantsEn.REGISTER_SUCCESS);
						}
				} catch (IOException e) {
					// TODO Auto-generated catch block
					// TODO Auto-generated catch block
					if(!LocaleContextHolder.getLocale().toString().equals("en"))
						errs.add(ErrorContantsTr.IS_NOT_UPLOADED);
						else
							errs.add(ErrorContantsEn.IS_NOT_UPLOADED);
				
				
				
					e.printStackTrace();
					
				}
				 
			}else{
				if(!LocaleContextHolder.getLocale().toString().equals("en"))
					errs.add(ErrorContantsTr.PASSWORD_IS_NOT_CONFIRMED);
					else
						errs.add(ErrorContantsEn.PASSWORD_IS_NOT_CONFIRMED);
			}
		}
		
	  map.addAttribute("errs",errs);
		
	    
		
	return "authorPage";
	}
	
	@RequestMapping(value="/authorRegister", method = RequestMethod.GET)
	public String getAuthorPageFromRegister(){
		
		
	return "redirect:/author";
	}
	
	@RequestMapping(value="/authorBooks", method = RequestMethod.POST)
	public String getAuthorBookFromLogin(ModelMap map,HttpServletRequest request){
		HttpSession session=request.getSession();
		if(session!=null){
			 Author author=(Author)request.getSession().getAttribute("author");
			    if(author == null){
			    	return "redirect:/author";
			    }else{
			    	  map.addAttribute("author",author);
			    }
			  
		}
	return "authorBookPage";
	}
	
	@RequestMapping(value="/authorBooks", method = RequestMethod.GET)
	public String getAuthorBookFromGET(ModelMap map,HttpServletRequest request) throws Exception{
		InputStream is = request.getInputStream();
		byte b[] = IOUtils.toByteArray(is);
		
		byte[] encodeBase64 = Base64.encode(b);
		String base64DataString = new String(encodeBase64 , "UTF-8");
		
		HttpSession session=request.getSession();
		if(session!=null){
			 Author author=(Author)request.getSession().getAttribute("author");
			    if(author == null){
			    	return "redirect:/author";
			    }else{
			    	  map.addAttribute("author",author);
			    }
			  
		}
	return "authorBookPage";
	}
	
	@RequestMapping(value="/createBook",method=RequestMethod.POST)
	public String createBookPost(@RequestParam("bookImage")MultipartFile imageFile,@RequestParam("bookPdf")MultipartFile bookFile,
			@RequestParam("bookName")String bookName,@RequestParam("bookType")String bookType,ModelMap map,HttpServletRequest req) throws Exception{
		Book book=new Book();
		book.setName(bookName);
		book.setType(bookType);
		book.setBookFile(bookFile.getBytes());
		book.setImageData(imageFile.getBytes());
		HttpSession session=req.getSession();
	
		if(session!=null){
			Author author=(Author)session.getAttribute("author");
			if(author!=null){
				int authorId=author.getId();
				Boolean isCreate=authorServiceImpl.addBook(book, authorId);
				author.getBooks().add(book);
				map.addAttribute("author",author);
			}else{
				return "redirect:/author";
			}
		}
		return "authorBookPage";
	
	}
	
	@RequestMapping(value="/createBook",method=RequestMethod.GET)
   public String createBookGet(){
	return "redirect:/authorBooks";
	}
	
	
	
	
}
