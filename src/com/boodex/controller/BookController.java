package com.boodex.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.boodex.domain.Author;
import com.boodex.domain.Book;
import com.boodex.domain.Content;
import com.boodex.service.AuthorService;
import com.boodex.service.BookService;

import antlr.collections.List;

@Controller
public class BookController {

	@Autowired
	AuthorService authorServiceImpl;
	
	@Autowired
	BookService bookServiceImpl;
	
	@RequestMapping("/delete{id}")
	public String deleteBook(@RequestParam("id")int id,HttpServletRequest request,ModelMap map){
		
		HttpSession session=request.getSession();
		if(session!=null){
			 Author author=(Author)request.getSession().getAttribute("author");
			    if(author == null){
			    	return "redirect:/author";
			    }else{
			    	try{
			    		Boolean isDeleteBook=authorServiceImpl.deleteBook(id);
			    	}catch(Exception e){
			    		return "redirect:/author";
			    	}
			    	  
			    	  author.getBooks().clear();
			    	  author.setBooks(authorServiceImpl.getBooks(author.getId()));
			    	  map.addAttribute("author",author);
			    }
			  
		}

		return "authorBookPage";
	}
	
	@RequestMapping("/contents{bookId}")
	public String contentsBook(@RequestParam("bookId")int bookId,HttpServletRequest request,ModelMap map){
		
		HttpSession session=request.getSession();
		if(session!=null){
			 Author author=(Author)request.getSession().getAttribute("author");
			    if(author == null){
			    	return "redirect:/author";
			    }else{
			    	try{
			    		
			    		java.util.List<Content> bookContents = bookServiceImpl.getContents(bookId);
			    		map.addAttribute("contents",bookContents);
			    		
			    	}catch(Exception e){
			    		return "redirect:/author";
			    	}finally{
			    		try{
			    			Book book=bookServiceImpl.getBook(bookId);
				    		map.addAttribute("book",book);
			    		}catch(Exception e){
			    			return "redirect:/author";
			    		}
			    		
			    	}
			    	
			    
			    	  
			    }
			  
		}

		return "bookContentPage";
	}
}
